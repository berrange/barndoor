Barndoor Control Readme
=======================

This project contains an arduino program suitable for controlling an
astrophotography barndoor mount. The circuit being controlled is
described in the blog post:

* http://fstop138.berrange.com/2014/01/building-an-barn-door-mount-part-1-arduino-stepper-motor-control/

The code design is based on the maths calculations documented at

* http://fstop138.berrange.com/2014/01/building-an-barn-door-mount-part-2-calculating-mount-movements/

The code is provided under the terms of the GPL version 2.1 or later.

For any questions / bug reports / patches send email to

* dan-barndoor@berrange.com

The master GIT repository is

* http://gitlab.com/berrange/barndoor

The libraries subdirectory bundles the 3rd party libraries that are
used by the code, to avoid reliance on external sites that may go
away. To install the libraries do the following

```
    cd libraries
    zip -r AccelStepper.zip AccelStepper/
    zip -r FSM.zip FSM/
```

Then go into the Arduino IDE and follow the menu

```
    Sketch -> Include Library -> Add .ZIP Library...
```

and select one of the just created ZIP files to import. Repeat this
for the other required library.
